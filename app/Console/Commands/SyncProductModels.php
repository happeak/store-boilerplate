<?php

namespace App\Console\Commands;

use App\Models\Shop\ProductModel;
use App\Services\Synchronizer;
use Happeak\ApiClient;
use Illuminate\Console\Command;

class SyncProductModels extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:models {--ids=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация данных моделей товаров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ApiClient    $happeak
     * @param Synchronizer $sync
     *
     * @return mixed
     */
    public function handle(ApiClient $happeak, Synchronizer $sync)
    {
        $ids = $this->option('ids')
            ? array_filter(explode(',', $this->option('ids')))
            : ProductModel::all()->pluck('id')->toArray();

        $models = $happeak->models->filterByIds($ids);
        $options = $happeak->options->filterByModelIds($ids);

        if ($sync->models($models)) {
            $this->info('Обновлено: модели товаров');
        }
        if ($sync->model_options($options)) {
            $this->info('Обновлено: характеристики моделей товаров');
        }
    }
}
