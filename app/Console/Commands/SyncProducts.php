<?php

namespace App\Console\Commands;

use App\Models\Shop\Product;
use App\Services\Synchronizer;
use Happeak\ApiClient;
use Illuminate\Console\Command;

class SyncProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products {--ids=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация товаров с happeak.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ApiClient    $happeak
     * @param Synchronizer $sync
     *
     * @return mixed
     */
    public function handle(ApiClient $happeak, Synchronizer $sync)
    {
        $ids = $this->option('ids')
            ? array_filter(explode(',', $this->option('ids')))
            : Product::all()->pluck('id')->toArray();

        $chunks = array_chunk($ids, 100);
        foreach ($chunks as $ids) {
            $products = $happeak->products->filterByIds($ids);
        }

        if ($sync->products($products)) {
            $this->info('Обновлено: товары');
        }
    }
}
