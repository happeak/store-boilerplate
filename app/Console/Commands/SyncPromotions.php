<?php

namespace App\Console\Commands;

use App\Services\Synchronizer;
use Happeak\ApiClient;
use Illuminate\Console\Command;

class SyncPromotions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:promotions';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация акций';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ApiClient    $happeak
     *
     * @param Synchronizer $sync
     *
     * @return mixed
     */
    public function handle(ApiClient $happeak, Synchronizer $sync)
    {
        $promotions = $happeak->promotions->filterByCriteria([
            'only_active' => true,
        ]);

        $sync->promotions($promotions);

        $this->info('synced: ' . count($promotions['promotions']) . ' promotions');
    }
}
