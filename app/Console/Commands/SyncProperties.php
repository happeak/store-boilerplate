<?php

namespace App\Console\Commands;

use App\Models\Shop\Product;
use App\Models\Shop\ProductModel;
use App\Services\Synchronizer;
use Happeak\ApiClient;
use Illuminate\Console\Command;

class SyncProperties extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:options {--type=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация справочника свойств товаров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ApiClient    $happeak
     * @param Synchronizer $sync
     *
     * @return mixed
     */
    public function handle(ApiClient $happeak, Synchronizer $sync)
    {
        if ($this->option('type') == 'models') {
            $modelIds = ProductModel::all()->pluck('remote_id')->toArray();

            $options = $happeak->options->filterByModelIds($modelIds);

            $sync->model_options($options);
            $this->info('synced: model properties');
        }
        if ($this->option('type') == 'products') {
            $productIds = Product::all()->pluck('remote_id')->toArray();

            $options = $happeak->options->filterByProductIds($productIds);

            $sync->product_options($options);
            $this->info('synced: product properties');
        }
        if (!$this->option('type')) {
            $options = $happeak->options->all();
            $sync->options($options);
            $this->info('synced: properties dictionary');
        };
    }
}
