<?php

namespace App\Console;

use App\Console\Commands\SitemapGenerate;
use App\Console\Commands\SyncProductModels;
use App\Console\Commands\SyncProducts;
use App\Console\Commands\SyncPromotions;
use App\Console\Commands\SyncProperties;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        SyncProductModels::class,
        SyncProducts::class,
        SyncProperties::class,
        SyncPromotions::class,
        SitemapGenerate::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     *
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
