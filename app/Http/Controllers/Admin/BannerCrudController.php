<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 05.03.18
 * Time: 15:11
 */

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\BannerCrudRequest as StoreRequest;
use App\Http\Requests\BannerCrudRequest as UpdateRequest;

class BannerCrudController extends CrudController
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Banner::class);
        $this->crud->setRoute('admin/banners');
        $this->crud->setEntityNameStrings('баннер', 'баннеры');

        $this->crud->setColumns([
            'id',
            [
                'name'  => 'title',
                'label' => 'Заголовок',
            ],
            [
                'name'  => 'position',
                'label' => 'Позиция',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Заголовок',
                'name'  => 'title',
            ],
            [
                'label' => 'Описание',
                'name'  => 'description',
            ],
            [
                'label' => 'Ссылка',
                'name'  => 'link',
            ],
            [
                'label' => 'Позиция',
                'name'  => 'position',
            ],
            [
                'label' => 'Порядковый номер',
                'name'  => 'sort',
                'type'  => 'number',
            ],
            [
                'label'        => 'Баннер',
                'name'         => 'banner',
                'type'         => 'image',
                'upload'       => true,
                'crop'         => false,
                'aspect_ratio' => 1,
            ],
            [
                'label' => 'Опубликовать',
                'name'  => 'is_public',
                'type'  => 'checkbox',
            ],
            [
                'label'                   => 'Дата публикации',
                'name'                    => 'publish_at',
                'type'                    => 'datetime_picker',
                'datetime_picker_options' => [
                    'format'   => 'DD/MM/YYYY HH:mm',
                    'language' => 'en',
                ],
                'allows_null'             => true,
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}