<?php

namespace App\Http\Controllers\Admin;

use App\Models\Shop\Brand;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\BrandCrudRequest as StoreRequest;
use App\Http\Requests\Admin\BrandCrudRequest as UpdateRequest;

class BrandCrudController extends CrudController
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Brand::class);
        $this->crud->setRoute('admin/brands');
        $this->crud->setEntityNameStrings('brand', 'brands');

        $this->crud->setColumns([
            'id',
            [
                'label' => 'Имя бренда',
                'name'  => 'name',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Наименование',
                'name'  => 'name',
            ],
            [
                'label' => 'ID на happeak.ru',
                'name'  => 'remote_id',
            ],
            [
                'label' => 'Описание',
                'name'  => 'description',
                'type'  => 'summernote',
            ],
            [
                'label'        => 'Логотип',
                'name'         => 'image',
                'type'         => 'image',
                'upload'       => true,
                'crop'         => false,
                'aspect_ratio' => 0,
                'prefix'       => 'uploads/images/brand/',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
