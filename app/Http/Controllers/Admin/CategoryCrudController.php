<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\CategoryCrudRequest as StoreRequest;
use App\Http\Requests\Admin\CategoryCrudRequest as UpdateRequest;
use App\Models\Shop\Category;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class CategoryCrudController extends CrudController
{

    public function setup()
    {
        $this->crud->setModel(Category::class);
        $this->crud->setRoute('admin/categories');
        $this->crud->setEntityNameStrings('category', 'categories');

        $this->crud->setColumns([
            'id',
            [
                'label' => 'Наименование',
                'name'  => 'name',
            ],
            [
                'label' => 'Позиция',
                'name'  => 'position',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Наименование RU',
                'name'  => 'name',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'URL',
                'name'  => 'slug',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Позиция',
                'name'  => 'position',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'ID родительской категории',
                'name'  => 'parent_id',
                'tab'   => 'Общее',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
