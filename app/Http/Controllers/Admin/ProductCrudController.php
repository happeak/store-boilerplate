<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProductCrudRequest as StoreRequest;
use App\Http\Requests\Admin\ProductCrudRequest as UpdateRequest;
use App\Models\Shop\Brand;
use App\Models\Shop\BrandColor;
use App\Models\Shop\Category;
use App\Models\Shop\Product;
use App\Models\Shop\ProductModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class ProductCrudController extends CrudController
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(Product::class);
        $this->crud->setRoute('admin/products');
        $this->crud->setEntityNameStrings('product', 'products');

        $this->crud->enableAjaxTable();

        $this->crud->orderBy('name', 'asc');
        $this->crud->orderBy('is_public', 'desc');
        $this->crud->orderBy('is_blocked', 'asc');

        $this->crud->setColumns([
            'id',
            [
                'label' => 'Товар',
                'name'  => 'name',
            ],
            [
                'label' => 'Цена',
                'name'  => 'price',
            ],
            [
                'label' => 'Статус публикации',
                'name'  => 'is_public',
                'type'  => 'check',
            ],
            [
                'label' => 'Товар заблокирован',
                'name'  => 'is_blocked',
                'type'  => 'check',
            ],
        ]);

        $this->crud->addFilter([
            'type'  => 'dropdown',
            'name'  => 'is_blocked',
            'label' => 'Товар заблокирован',
        ], [
            0 => 'Нет',
            1 => 'Да',
        ], function ($value) {
            $this->crud->addClause('where', 'is_blocked', $value);
        });

        $this->crud->addFilter([
            'type'  => 'dropdown',
            'name'  => 'is_public',
            'label' => 'Статус публикации',
        ], [
            0 => 'Снят с публикации',
            1 => 'Опубликован',
        ], function ($value) {
            $this->crud->addClause('where', 'is_public', $value);
        });

        $this->crud->addFields([
            [
                'label' => 'Наименование',
                'name'  => 'name',
                'tab'   => 'Общее',
            ],
            [
                'label'  => 'Цена',
                'name'   => 'price',
                'type'   => 'number',
                'prefix' => '€',
                'suffix' => '.00',
                'tab'    => 'Общее',
            ],
            [
                'label' => 'Описание',
                'name'  => 'description',
                'type'  => 'summernote',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'ID в happeak.ru',
                'name'  => 'remote_id',
                'type'  => 'number',
                'tab'   => 'Настройки',
            ],
            [
                'label' => 'Опубликован?',
                'name'  => 'is_public',
                'type'  => 'checkbox',
                'tab'   => 'Настройки',
            ],
            [
                'label' => 'Заблокирован?',
                'name'  => 'is_blocked',
                'type'  => 'checkbox',
                'tab'   => 'Настройки',
            ],
            [
                'label' => 'Товар из прошлого?',
                'name'  => 'is_past',
                'type'  => 'checkbox',
                'tab'   => 'Настройки',
            ],
            [
                'label' => 'Остаток',
                'name'  => 'stock_count',
                'type'  => 'number',
                'tab'   => 'Общее',
            ],
            [
                'label'     => 'Бренд',
                'name'      => 'brand_id',
                'type'      => 'select2',
                'entity'    => 'brand',
                'model'     => Brand::class,
                'attribute' => 'name',
                'tab'       => 'Взаимосвязи',
            ],
            [
                'label'     => 'Категория',
                'name'      => 'category_id',
                'type'      => 'select2',
                'entity'    => 'category',
                'model'     => Category::class,
                'attribute' => 'name',
                'tab'       => 'Взаимосвязи',
            ],
            [
                'label'     => 'Модель',
                'name'      => 'model_id',
                'type'      => 'select2',
                'entity'    => 'product_model',
                'model'     => ProductModel::class,
                'attribute' => 'name',
                'tab'       => 'Взаимосвязи',
            ],
            [
                'label'  => 'Изображения',
                'name'   => 'images',
                'type'   => 'upload_multiple',
                'upload' => true,
                'disk'   => 'uploads',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
