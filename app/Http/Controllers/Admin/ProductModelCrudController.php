<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\ProductModelCrudRequest as StoreRequest;
use App\Http\Requests\Admin\ProductModelCrudRequest as UpdateRequest;
use App\Models\Shop\Brand;
use App\Models\Shop\BrandModelGroup;
use App\Models\Shop\Category;
use App\Models\Shop\ProductModel;
use Backpack\CRUD\app\Http\Controllers\CrudController;

class ProductModelCrudController extends CrudController
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(ProductModel::class);
        $this->crud->setRoute('admin/models');
        $this->crud->setEntityNameStrings('model', 'models');

        $this->crud->enableAjaxTable();

        $this->crud->orderBy('name', 'asc');

        $this->crud->setColumns([
            [
                'label' => 'ID',
                'name'  => 'id',
            ],
            [
                'label' => 'Наименование модели',
                'name'  => 'name',
            ],
            [
                'label'     => 'Категория',
                'name'      => 'category_id',
                'type'      => 'select',
                'entity'    => 'category',
                'model'     => Category::class,
                'attribute' => 'name',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'Полное название модели',
                'name'  => 'name',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Наименование модели',
                'name'  => 'name_model',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Наименование бренда',
                'name'  => 'name_brand',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Наименование типа',
                'name'  => 'name_type',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Заголовок модели',
                'name'  => 'name_title',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'Наименование группы',
                'name'  => 'name_group',
                'tab'   => 'Общее',
            ],
            [
                'label' => 'ID в happeak . ru',
                'name'  => 'remote_id',
                'tab'   => 'Общее',
            ],
            [
                'label'     => 'Бренд',
                'name'      => 'brand_id',
                'type'      => 'select2',
                'entity'    => 'brand',
                'model'     => Brand::class,
                'attribute' => 'name',
                'tab'       => 'Общее',
            ],
            [
                'label'     => 'Категория',
                'name'      => 'category_id',
                'type'      => 'select2',
                'entity'    => 'category',
                'model'     => Category::class,
                'attribute' => 'name',
                'tab'       => 'Общее',
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        return parent::storeCrud();
    }

    public function update(UpdateRequest $request)
    {
        return parent::updateCrud();
    }
}
