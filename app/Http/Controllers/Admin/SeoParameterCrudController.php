<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 26.03.18
 * Time: 13:32
 */

namespace App\Http\Controllers\Admin;

use App\Models\SeoParameter;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\Admin\SeoParameterCrudRequest as StoreRequest;
use App\Http\Requests\Admin\SeoParameterCrudRequest as UpdateRequest;

class SeoParameterCrudController extends CrudController
{

    /**
     * @throws \Exception
     */
    public function setup()
    {
        $this->crud->setModel(SeoParameter::class);
        $this->crud->setRoute('admin/seo_parameters');
        $this->crud->setEntityNameStrings('seo_parameter', 'seo_parameters');

        $this->crud->setColumns([
            [
                'label' => 'URL',
                'name'  => 'url',
            ],
            [
                'label' => 'Title',
                'name'  => 'title',
            ],
            [
                'label' => 'Description',
                'name'  => 'description',
            ],
            [
                'label' => 'Keywords',
                'name'  => 'keywords',
            ],
        ]);

        $this->crud->addFields([
            [
                'label' => 'URL',
                'name'  => 'url',
            ],
            [
                'label' => 'Title',
                'name'  => 'title',
            ],
            [
                'label' => 'Description',
                'name'  => 'description',
                'type'  => 'textarea',
            ],
            [
                'label' => 'Keywords',
                'name'  => 'keywords',
            ],
        ]);
    }

    /**
     * @param UpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        return parent::storeCrud($request);
    }

    /**
     * @param UpdateRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        return parent::updateCrud($request);
    }
}