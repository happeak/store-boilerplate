<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 21.03.18
 * Time: 15:29
 */

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class OauthController extends Controller
{

    use AuthenticatesUsers;

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @param $provider
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @param $provider
     *
     * @return Response
     * @throws \Exception
     */
    public function handleProviderCallback(string $provider)
    {
        if ($provider !== 'google')
            abort(500, 'Oauth provider is not supported');

        $socialiteUser = Socialite::driver($provider)->stateless()->user();

        $user = Admin::firstOrCreate(
            [
                'email' => $socialiteUser->email,
            ],
            [
                'name'     => $socialiteUser->name,
                'password' => Hash::make(uniqid(mt_rand(), true)),
            ]
        );

        Auth::login($user, true);

        return redirect()->route('backpack.dashboard');
    }
}