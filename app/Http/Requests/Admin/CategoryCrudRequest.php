<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\CrudRequest;

class CategoryCrudRequest extends CrudRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required',
            'position' => 'required',
        ];
    }
}
