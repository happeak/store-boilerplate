<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\CrudRequest;

class ProductCrudRequest extends CrudRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'required|max:255',
            'brand_id'    => 'required|int',
            'is_public'   => 'required|bool',
            'is_blocked'  => 'required|bool',
            'is_past'     => 'required|bool',
            'stock_count' => 'required|int',
        ];
    }
}
