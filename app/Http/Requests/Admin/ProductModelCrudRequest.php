<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\CrudRequest;

class ProductModelCrudRequest extends CrudRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_model'  => 'required',
            'brand_id'    => 'required|int',
            'category_id' => 'required|int',
        ];
    }
}
