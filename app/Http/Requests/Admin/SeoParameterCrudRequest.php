<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\CrudRequest;

class SeoParameterCrudRequest extends CrudRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'url'         => 'required|string|max:255',
            'title'       => 'required|string|max:255',
            'description' => 'required|string',
            'keywords'    => 'nullable|string|max:255',
        ];
    }
}
