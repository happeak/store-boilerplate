<?php

namespace App\Http\Requests;

class BannerCrudRequest extends CrudRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'  => 'string|max:255',
            'link'   => 'string|max:255',
            'banner' => 'required',
        ];
    }
}
