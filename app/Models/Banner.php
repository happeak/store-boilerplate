<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Banner
 * @package App\Models
 *
 * @property string  $title
 * @property string  $description
 * @property string  $link
 * @property string  $banner
 * @property boolean $is_public
 * @property Carbon  $publish_at
 * @property string  $position
 * @property integer $sort
 */
class Banner extends Model
{

    use CrudTrait;

    /**
     * @var string
     */
    protected $table = 'shop_banners';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'is_public'  => 'boolean',
        'publish_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();
        static::deleting(function ($obj) {
            \Storage::disk('public')->delete($obj->banner);
        });
    }

    /**
     * Banner mutator
     *
     * @param $value
     */
    public function setBannerAttribute($value)
    {
        $attrName = 'banner';
        $disk = 'public';
        $destinationFolder = 'banners';

        if ($value == null) {
            \Storage::disk($disk)->delete($this->{$attrName});
            $this->attributes[$attrName] = null;
        }

        if (starts_with($value, 'data:image')) {
            $image = \Image::make($value);
            $filename = md5($value . time()) . '.jpg';
            \Storage::disk($disk)->put($destinationFolder . '/' . $filename, $image->stream());
            $this->attributes[$attrName] = $destinationFolder . '/' . $filename;
        }
    }
}
