<?php

namespace App\Models;

use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SeoParameter
 * @package App\Models
 *
 * @property string $url
 * @property string $title
 * @property string $description
 * @property string $keywords
 */
class SeoParameter extends Model
{

    use CrudTrait;

    /**
     * @var string
     */
    protected $table = 'shop_seo_parameters';

    /**
     * @var array
     */
    protected $guarded = [];
}
