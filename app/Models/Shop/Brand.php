<?php

namespace App\Models\Shop;

use Backpack\CRUD\CrudTrait;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Brand
 *
 * @package App\Models\Shop
 * @property string              $name
 * @property int                 $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static Builder|\App\Models\Shop\Brand whereCreatedAt($value)
 * @method static Builder|\App\Models\Shop\Brand whereId($value)
 * @method static Builder|\App\Models\Shop\Brand whereName($value)
 * @method static Builder|\App\Models\Shop\Brand whereUpdatedAt($value)
 */
class Brand extends Model
{

    use CrudTrait;

    protected $table = 'shop_brands';

    protected $guarded = [];
}
