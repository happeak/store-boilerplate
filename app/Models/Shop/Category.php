<?php

namespace App\Models\Shop;

use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\Category
 *
 * @property int $id
 * @property int|null $parent_id
 * @property string|null $name
 * @property string|null $slug
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $synced_at
 * @property int $position
 * @property-read Collection|\App\Models\Shop\Product[] $products
 * @method static Builder|\App\Models\Shop\Category findSimilarSlugs($attribute, $config, $slug)
 * @method static Builder|\App\Models\Shop\Category whereCreatedAt($value)
 * @method static Builder|\App\Models\Shop\Category whereId($value)
 * @method static Builder|\App\Models\Shop\Category whereName($value)
 * @method static Builder|\App\Models\Shop\Category whereParentId($value)
 * @method static Builder|\App\Models\Shop\Category wherePosition($value)
 * @method static Builder|\App\Models\Shop\Category whereSlug($value)
 * @method static Builder|\App\Models\Shop\Category whereSyncedAt($value)
 * @method static Builder|\App\Models\Shop\Category whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read Collection|\App\Models\Shop\Category[] $children
 * @property-read \App\Models\Shop\Category|null $parent
 * @method static Builder|\App\Models\Shop\Category findOneByRemoteId($id)
 */
class Category extends Model
{

    use CrudTrait, Sluggable;

    /**
     * @const CATEGORY_CARSEATS
     */
    const CATEGORY_CARSEATS = 1;

    /**
     * @const CATEGORY_STROLLERS
     */
    const CATEGORY_STROLLERS = 2;

    protected $table = 'shop_categories';
    protected $guarded = [];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Родительская категория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Category::class, 'parent_id');
    }

    /**
     * Дочерние категории
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

    /**
     * Category products
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class, 'category_id');
    }
}
