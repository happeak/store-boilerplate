<?php

namespace App\Models\Shop;

use App\Models\Shop\Brand;
use App\Models\Shop\Category;
use App\Models\Shop\ProductModel;
use Backpack\CRUD\CrudTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\Product
 *
 * @property int                                                                       $id
 * @property \Carbon\Carbon|null                                                       $created_at
 * @property \Carbon\Carbon|null                                                       $updated_at
 * @property int                                                                       $brand_id
 * @property int|null                                                                  $category_id
 * @property int|null                                                                  $model_id
 * @property int|null                                                                  $model_group_id
 * @property int|null                                                                  $collection_id
 * @property int|null                                                                  $color_id
 * @property int|null                                                                  $model_year
 * @property string|null                                                               $name_model
 * @property string|null                                                               $name_brand
 * @property string|null                                                               $name_type
 * @property string|null                                                               $name_type_gen
 * @property string|null                                                               $name_title
 * @property string|null                                                               $name_color
 * @property string|null                                                               $name_group
 * @property string|null                                                               $description
 * @property int|null                                                                  $remote_id
 * @property string|null                                                               $synced_at
 * @property int                                                                       $is_public
 * @property int                                                                       $is_blocked
 * @property int                                                                       $is_past
 * @property float|null                                                                $price
 * @property int                                                                       $stock_count
 * @property string|null                                                               $stock_date
 * @property array                                                                     $images
 * @property string|null                                                               $name
 * @property string|null                                                               $slug
 * @property string|null                                                               $thumbnail_path
 * @property-read Brand                                                                $brand
 * @property-read Category|null                                                        $category
 * @property-read ProductModel|null                                                    $model
 * @method static Builder|\App\Models\Shop\Product findSimilarSlugs($attribute, $config, $slug)
 * @method static Builder|\App\Models\Shop\Product whereBrandId($value)
 * @method static Builder|\App\Models\Shop\Product whereCategoryId($value)
 * @method static Builder|\App\Models\Shop\Product whereCollectionId($value)
 * @method static Builder|\App\Models\Shop\Product whereColorId($value)
 * @method static Builder|\App\Models\Shop\Product whereCreatedAt($value)
 * @method static Builder|\App\Models\Shop\Product whereDescription($value)
 * @method static Builder|\App\Models\Shop\Product whereId($value)
 * @method static Builder|\App\Models\Shop\Product whereImages($value)
 * @method static Builder|\App\Models\Shop\Product whereIsBlocked($value)
 * @method static Builder|\App\Models\Shop\Product whereIsPast($value)
 * @method static Builder|\App\Models\Shop\Product whereIsPublic($value)
 * @method static Builder|\App\Models\Shop\Product whereModelGroupId($value)
 * @method static Builder|\App\Models\Shop\Product whereModelId($value)
 * @method static Builder|\App\Models\Shop\Product whereModelYear($value)
 * @method static Builder|\App\Models\Shop\Product whereName($value)
 * @method static Builder|\App\Models\Shop\Product whereNameBrand($value)
 * @method static Builder|\App\Models\Shop\Product whereNameColor($value)
 * @method static Builder|\App\Models\Shop\Product whereNameGroup($value)
 * @method static Builder|\App\Models\Shop\Product whereNameModel($value)
 * @method static Builder|\App\Models\Shop\Product whereNameTitle($value)
 * @method static Builder|\App\Models\Shop\Product whereNameType($value)
 * @method static Builder|\App\Models\Shop\Product whereNameTypeGen($value)
 * @method static Builder|\App\Models\Shop\Product wherePrice($value)
 * @method static Builder|\App\Models\Shop\Product whereRemoteId($value)
 * @method static Builder|\App\Models\Shop\Product whereSlug($value)
 * @method static Builder|\App\Models\Shop\Product whereStockCount($value)
 * @method static Builder|\App\Models\Shop\Product whereStockDate($value)
 * @method static Builder|\App\Models\Shop\Product whereSyncedAt($value)
 * @method static Builder|\App\Models\Shop\Product whereThumbnailPath($value)
 * @method static Builder|\App\Models\Shop\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null                                                                  $subcategory_id
 * @property float|null                                                                $base_price
 * @method static Builder|\App\Models\Shop\Product whereBasePrice($value)
 * @method static Builder|\App\Models\Shop\Product whereSubcategoryId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Shop\Property[] $properties
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Product filterByBlocked($status = true)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Shop\Product filterByPublic($status = true)
 */
class Product extends Model
{

    use CrudTrait, Sluggable;

    protected $casts = [
        'images' => 'array',
    ];

    protected $table = 'shop_products';

    protected $guarded = [];

    /**
     * Модель
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo(ProductModel::class, 'model_id');
    }

    /**
     * Бренд
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    /**
     * Категория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Свойства товара
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany(PropertyValue::class, 'object_id', 'object_id');
    }

    /**
     * Фильтр по опубликованным
     *
     * @param      $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeFilterByPublic($query, bool $status = true)
    {
        return $query->where('is_public', $status);
    }

    /**
     * Фильтр по заблокированным
     *
     * @param      $query
     * @param bool $status
     *
     * @return mixed
     */
    public function scopeFilterByBlocked($query, bool $status = true)
    {
        return $query->where('is_blocked', $status);
    }

    /**
     * Фильтр по наличию в акциях
     *
     * @param      $query
     * @param bool $hasPromotion
     *
     * @return mixed
     */
    public function scopeFilterByHasPromotion($query, bool $hasPromotion = true)
    {
        $promotionProductIds = Promotion::filterByPublic()->pluck('products')->toArray();
        $promotionProductIds = call_user_func_array('array_merge', $promotionProductIds);

        return $hasPromotion
            ? $query->whereIn('id', array_unique($promotionProductIds))
            : $query->whereNotIn('id', array_unique($promotionProductIds));
    }

    /**
     * Sluggable method
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }
}
