<?php

namespace App\Models\Shop;

use App\Models\Shop\Brand;
use App\Models\Shop\Category;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\ProductModel
 *
 * @property int                                                      $id
 * @property \Carbon\Carbon|null                                      $created_at
 * @property \Carbon\Carbon|null                                      $updated_at
 * @property int                                                      $brand_id
 * @property int|null                                                 $model_group_id
 * @property int|null                                                 $collection_id
 * @property int                                                      $category_id
 * @property int|null                                                 $model_year
 * @property string|null                                              $name_model
 * @property string|null                                              $name_brand
 * @property string|null                                              $name_type
 * @property string|null                                              $name_type_gen
 * @property string|null                                              $name_title
 * @property string|null                                              $name_group
 * @property int|null                                                 $remote_id
 * @property string|null                                              $synced_at
 * @property string|null                                              $name
 * @property string|null                                              $description
 * @property int                                                      $is_public
 * @property int                                                      $is_blocked
 * @property-read Brand                                               $brand
 * @property-read Category                                            $category
 * @method static Builder|ProductModel whereBrandId($value)
 * @method static Builder|ProductModel whereCategoryId($value)
 * @method static Builder|ProductModel whereCollectionId($value)
 * @method static Builder|ProductModel whereCreatedAt($value)
 * @method static Builder|ProductModel whereDescription($value)
 * @method static Builder|ProductModel whereId($value)
 * @method static Builder|ProductModel whereIsBlocked($value)
 * @method static Builder|ProductModel whereIsPublic($value)
 * @method static Builder|ProductModel whereModelGroupId($value)
 * @method static Builder|ProductModel whereModelYear($value)
 * @method static Builder|ProductModel whereName($value)
 * @method static Builder|ProductModel whereNameBrand($value)
 * @method static Builder|ProductModel whereNameGroup($value)
 * @method static Builder|ProductModel whereNameModel($value)
 * @method static Builder|ProductModel whereNameTitle($value)
 * @method static Builder|ProductModel whereNameType($value)
 * @method static Builder|ProductModel whereNameTypeGen($value)
 * @method static Builder|ProductModel whereRemoteId($value)
 * @method static Builder|ProductModel whereSyncedAt($value)
 * @method static Builder|ProductModel whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null                                                 $subcategory_id
 * @property array                                                    $images
 * @method static Builder|ProductModel whereImages($value)
 * @method static Builder|ProductModel whereSubcategoryId($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|Property[] $properties
 */
class ProductModel extends Model
{

    use CrudTrait;

    /**
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var string
     */
    protected $table = 'shop_product_models';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'synced_at' => 'timestamp',
        'images'    => 'array',
    ];

    /**
     * Бренд
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }

    /**
     * Категория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * Свойства модели
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties()
    {
        return $this->hasMany(PropertyValue::class, 'property_id', 'object_id')
            ->where('destination', 'model');
    }
}
