<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Promotion
 * @package App\Models\Shop
 */
class Promotion extends Model
{

    /**
     * @var string
     */
    protected $table = 'shop_promotions';

    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @var array
     */
    protected $casts = [
        'start_at'  => 'datetime',
        'expire_at' => 'datetime',
        'is_public' => 'boolean',
        'products'  => 'array',
    ];

    /**
     * @param      $query
     * @param bool $isPublic
     *
     * @return mixed
     */
    public function scopeFilterByPublic($query, bool $isPublic = true)
    {
        return $query->where('is_public', $isPublic);
    }
}
