<?php

namespace App\Models\Shop;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Shop\Property
 *
 * @property int                            $id
 * @property \Carbon\Carbon|null            $created_at
 * @property \Carbon\Carbon|null            $updated_at
 * @property int                            $category_id
 * @property string                         $destination
 * @property string|null                    $unit
 * @property int                            $position
 * @property string|null                    $pattern
 * @property int                            $is_filter
 * @property string                         $value_type
 * @property string|null                    $values
 * @property string|null                    $default_value
 * @property string|null                    $group
 * @property string                         $name
 * @property-read \App\Models\Shop\Category $category
 * @method static Builder|Property whereCategoryId($value)
 * @method static Builder|Property whereCreatedAt($value)
 * @method static Builder|Property whereDefaultValue($value)
 * @method static Builder|Property whereDestination($value)
 * @method static Builder|Property whereGroup($value)
 * @method static Builder|Property whereId($value)
 * @method static Builder|Property whereIsFilter($value)
 * @method static Builder|Property whereName($value)
 * @method static Builder|Property wherePattern($value)
 * @method static Builder|Property wherePosition($value)
 * @method static Builder|Property whereUnit($value)
 * @method static Builder|Property whereUpdatedAt($value)
 * @method static Builder|Property whereValueType($value)
 * @method static Builder|Property whereValues($value)
 * @mixin \Eloquent
 * @method static Builder|Property filterByCategory($categoryId = 2)
 * @method static Builder|Property filterByModel()
 * @method static Builder|Property filterByProduct()
 * @property int|null $brand_id
 * @method static Builder|Property getCollections()
 * @method static Builder|Property getModels()
 * @method static Builder|Property whereBrandId($value)
 */
class Property extends Model
{

    /**
     * @var string $table
     */
    protected $table = 'shop_properties';

    /**
     * @var array $guarded
     */
    protected $guarded = [];

    /**
     * @var array $casts
     */
    protected $casts = [
        'values' => 'array',
    ];

    /**
     * Категория
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(PropertyValue::class, 'property_id');
    }

    /**
     * Фильтр по характеристикам модели
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeFilterByModel($query)
    {
        return $query->where('destination', 'model');
    }

    /**
     * Фильтр по характеристикам товара
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeFilterByProduct($query)
    {
        return $query->where('destination', 'product');
    }

    /**
     * Фильтр по категории
     *
     * @param     $query
     * @param int $categoryId
     *
     * @return mixed
     */
    public function scopeFilterByCategory($query, int $categoryId = 2)
    {
        return $query->where('category_id', $categoryId);
    }
}
