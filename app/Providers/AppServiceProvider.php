<?php

namespace App\Providers;

use App\Services\Pechkin;
use App\Services\Seo;
use App\Services\Synchronizer;
use Illuminate\Http\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(Pechkin::class, function () {
            return new Pechkin();
        });

        $this->app->bind(Synchronizer::class, function () {
            return new Synchronizer();
        });

        $this->app->bind(Seo::class, function () {
            $request = app(Request::class);

            return new Seo($request);
        });
    }
}
