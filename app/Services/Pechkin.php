<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 20.10.17
 * Time: 17:56
 */

namespace App\Services;

use App\Models\User\User;
use HappeakApi\Pechkin\Api;
use HappeakApi\Pechkin\Endpoint;

class Pechkin
{

    protected $api = null;


    /**
     * Send notification
     *
     * @param string $id
     * @param array  $user
     * @param array  $params
     * @param array  $files
     *
     * @return mixed
     */
    public function notifySend($id, array $user, array $params = [], array $files = [])
    {
        $this->syncUser($user);

        $request = $this->getApi()
            ->request(Endpoint::notifySend($id))
            ->param($params)
            ->param('email', $user['email']);

        foreach ($files as $name => $path)
        {
            $request->attachment($path, $name);
        }

        return $request->call();
    }


    /**
     * Synchronize user
     *
     * @param array $user
     *
     * @return mixed
     */
    public function syncUser(array $user)
    {
        $map = [
            'first_name'  => 'first_name',
        ];

        $request = $this->getApi()->request(Endpoint::contactSync($user['email']));

        foreach ($map as $name => $field)
        {
            $request->param($name, $user->$field);
        }

        return $request->call();
    }


    /**
     * Returns Pechkin Api instance
     *
     * @return Api|null
     */
    protected function getApi()
    {
        if (null === $this->api)
        {
            $this->api = new Api([
                'project-id'  => config('pechkin.api_id'),
                'project-key' => config('pechkin.api_key'),
            ]);
        }

        return $this->api;
    }
}