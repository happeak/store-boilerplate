<?php

namespace App\Services;

use App\Models\SeoParameter;
use Illuminate\Http\Request;

class Seo
{

    /**
     * @var SeoParameter
     */
    protected $seoParameters;

    public function __construct(Request $request)
    {
        $this->seoParameters = SeoParameter::where('url', $request->path())->first();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->seoParameters ? $this->seoParameters->title : '';
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->seoParameters ? $this->seoParameters->description : '';
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->seoParameters ? $this->seoParameters->keywords : '';
    }
}