<?php
/**
 * Created by PhpStorm.
 * User: aleksei
 * Date: 31.01.18
 * Time: 13:58
 */

namespace App\Services;

use App\Models\Shop\Brand;
use App\Models\Shop\BrandCollection;
use App\Models\Shop\BrandColor;
use App\Models\Shop\BrandModelGroup;
use App\Models\Shop\Category;
use App\Models\Shop\Product;
use App\Models\Shop\ProductModel;
use App\Models\Shop\Promotion;
use App\Models\Shop\Property;
use App\Models\Shop\PropertyValue;
use Carbon\Carbon;

class Synchronizer
{

    /**
     * Синхронизация данных моделей
     *
     * @param array $models
     *
     * @return bool
     */
    public function models(array $models = []): bool
    {
        $brand = Brand::find(39);

        if (count($models) > 0) {

            $models = array_key_exists('models', $models) ? $models['models'] : $models;

            foreach ($models as $id => $model) {
                ProductModel::updateOrCreate([
                    'id' => $id,
                ], [
                    'brand_id'       => $brand->id,
                    'category_id'    => ($category = Category::find($model['category_id']))
                        ? $category->id
                        : null,
                    'subcategory_id' => ($model['subcategory_id'] && $subcategory =
                            Category::find($model['subcategory_id']))
                        ? $subcategory->id
                        : null,
                    'name'           => $model['name'],
                    'description'    => $model['description'],
                    'name_type'      => $model['name_type'],
                    'name_type_gen'  => $model['name_type_gen'],
                    'name_brand'     => $model['name_brand'],
                    'name_title'     => $model['name_title'],
                    'name_group'     => $model['name_group'],
                    'name_model'     => $model['name_model'],
                    'synced_at'      => Carbon::now(),
                    'images'         => array_map(function ($image) {
                        return str_replace('https://www.happeak.ru/images', '', $image);
                    }, $model['images']),
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация данных товаров
     *
     * @param array $products
     *
     * @return bool
     */
    public function products(array $products = [])
    {
        if (count($products) > 0) {

            $products = array_key_exists('products', $products) ? $products['products'] : $products;

            foreach ($products as $product) {
                Product::updateOrCreate([
                    'id' => $product['id'],
                ], [
                    'name'           => $product['name'],
                    'name_type'      => $product['name_type'],
                    'name_type_gen'  => $product['name_type_gen'],
                    'name_brand'     => $product['name_brand'],
                    'name_model'     => $product['name_model'],
                    'name_title'     => $product['name_title'],
                    'name_color'     => $product['name_color'],
                    'name_group'     => $product['name_group'],
                    'description'    => $product['description'],
                    'model_id'       => ($product['model_id'] && $model =
                            ProductModel::find($product['model_id']))
                        ? $model->id
                        : null,
                    'brand_id'       => Brand::find($product['brand_id'])->id,
                    'category_id'    => Category::find($product['category_id'])->id,
                    'subcategory_id' => ($product['subcategory_id'] && $subcategory =
                            Category::find($product['subcategory_id']))
                        ? $subcategory->id
                        : null,
                    'images'         => array_map(function ($image) {
                        return str_replace('https://www.happeak.ru/images', '', $image);
                    }, $product['images']),
                    'stock_date'     => $product['stock_date'],
                    'stock_count'    => $product['stock_count'],
                    'price'          => $product['price'],
                    'base_price'     => $product['base_price'],
                    'is_public'      => $product['is_public'],
                    'is_blocked'     => $product['is_blocked'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация справочника характеристик
     *
     * @param array $options
     *
     * @return bool
     */
    public function options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $option) {
                Property::updateOrCreate([
                    'id' => $option['id'],
                ], [
                    'name'          => $option['name'],
                    'group'         => $option['group'],
                    'unit'          => $option['unit'],
                    'destination'   => $option['destination'],
                    'default_value' => $option['default_value'],
                    'values'        => $option['values'],
                    'category_id'   => ($category = Category::find($option['category_id']))
                        ? $category->id
                        : null,
                    'is_filter'     => $option['is_filter'],
                    'position'      => $option['position'],
                ]);
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация характеристик моделей товаров
     *
     * @param array $options
     *
     * @return bool
     */
    public function model_options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $optionGroup) {
                foreach ($optionGroup as $option) {
                    $property = Property::find($option['option_id']);
                    $model = ProductModel::find($option['model_id']);

                    PropertyValue::updateOrCreate([
                        'object_id'   => $model->id,
                        'property_id' => $property->id,
                        'destination' => 'model',
                    ], [
                        'value'        => $option['value'],
                        'value_num'    => $option['value_int'],
                        'value_float'  => $option['value_float'],
                        'value_text'   => $option['value_text'],
                        'value_string' => $option['value_str'],
                        'name'         => $option['name'],
                        'group'        => $option['group'],
                        'unit'         => $option['unit'],
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация характеристик товаров
     *
     * @param array $options
     *
     * @return bool
     */
    public function product_options(array $options = [])
    {
        if (count($options) > 0) {
            foreach ($options['options'] as $optionGroup) {
                foreach ($optionGroup as $option) {
                    $property = Property::find($option['option_id']);
                    $product = Product::find($option['product_id']);

                    PropertyValue::updateOrCreate([
                        'object_id'   => $product->id,
                        'property_id' => $property->id,
                        'destination' => 'product',
                    ], [
                        'value'        => $option['value'],
                        'value_num'    => $option['value_int'],
                        'value_float'  => $option['value_float'],
                        'value_text'   => $option['value_text'],
                        'value_string' => $option['value_str'],
                        'name'         => $option['name'],
                        'group'        => $option['group'],
                        'unit'         => $option['unit'],
                    ]);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Синхронизация акций
     *
     * @param array $promotions
     *
     * @return bool
     */
    public function promotions(array $promotions = [])
    {
        if (count($promotions) > 0) {

            foreach ($promotions['promotions'] as $promotion) {
                Promotion::updateOrCreate([
                    'id' => $promotion['id'],
                ], [
                    'name'        => $promotion['name'],
                    'title'       => $promotion['title'],
                    'description' => $promotion['description'],
                    'is_public'   => $promotion['is_public'],
                    'start_at'    => $promotion['start_at'],
                    'expire_at'   => $promotion['expire_at'],
                    'products'    => $promotion['products'],
                ]);
            }

            return true;
        }

        return false;
    }
}