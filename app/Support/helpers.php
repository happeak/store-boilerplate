<?php

use App\Services\Seo;

if (!function_exists('seo_title')) {
    function seo_title()
    {
        return app()->make(Seo::class)->getTitle();
    }
}

if (!function_exists('seo_description')) {
    function seo_description()
    {
        return app()->make(Seo::class)->getDescription();
    }
}

if (!function_exists('seo_keywords')) {
    function seo_keywords()
    {
        return app()->make(Seo::class)->getKeywords();
    }
}