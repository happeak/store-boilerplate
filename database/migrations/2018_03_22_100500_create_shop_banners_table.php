<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopBannersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_banners', function (Blueprint $table) {
            $table->increments('id');

            $table->string('title', 255)->nullable();
            $table->text('description')->nullable();
            $table->string('link', 255)->nullable();
            $table->string('banner', 255);
            $table->boolean('is_public')->default(false);
            $table->dateTime('publish_at')->nullable();
            $table->string('position', 255)->nullable();
            $table->unsignedInteger('sort')->default(0)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_banners');
    }
}
