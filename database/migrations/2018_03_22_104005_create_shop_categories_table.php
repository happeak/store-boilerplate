<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopCategoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_categories', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->string('slug', 255);
            $table->unsignedInteger('position');

            $table->unsignedInteger('parent_id')->nullable();

            $table->timestamp('synced_at')->nullable();
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('shop_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_categories');
    }
}
