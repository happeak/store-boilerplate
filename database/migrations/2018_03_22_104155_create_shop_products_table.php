<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopProductsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255)->nullable();
            $table->string('name_brand', 255)->nullable();
            $table->string('name_model', 255)->nullable();
            $table->string('name_type', 255)->nullable();
            $table->string('name_type_gen', 255)->nullable();
            $table->string('name_title', 255)->nullable();
            $table->string('name_group', 255)->nullable();
            $table->string('name_color', 255)->nullable();

            $table->string('slug', 255);

            $table->text('description')->nullable();
            $table->json('images')->nullable();

            $table->decimal('base_price', 12, 2)->nullable();
            $table->decimal('price', 12, 2)->nullable();
            $table->unsignedInteger('stock_count')->default(0);
            $table->date('stock_date')->nullable();

            $table->boolean('is_public')->default(0);
            $table->boolean('is_blocked')->default(0);

            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('subcategory_id')->nullable();
            $table->unsignedInteger('model_id')->nullable();

            $table->timestamp('synced_at')->nullable();
            $table->timestamps();

            $table->foreign('brand_id')->references('id')->on('shop_brands');
            $table->foreign('category_id')->references('id')->on('shop_categories');
            $table->foreign('subcategory_id')->references('id')->on('shop_categories');
            $table->foreign('model_id')->references('id')->on('shop_product_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_products');
    }
}
