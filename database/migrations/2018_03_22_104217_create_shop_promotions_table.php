<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPromotionsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_promotions', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 255);
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->json('products')->nullable();
            $table->boolean('is_public')->default(0);

            $table->dateTime('start_at');
            $table->dateTime('expire_at');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_promotions');
    }
}
