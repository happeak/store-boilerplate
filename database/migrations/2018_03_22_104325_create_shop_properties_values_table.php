<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopPropertiesValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_properties_values', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('property_id');
            $table->unsignedInteger('object_id');
            $table->enum('destination', ['model', 'product'])->default('model');

            $table->decimal('value_num', 12,3)->nullable();
            $table->string('value_string', 255)->nullable();
            $table->text('value_text')->nullable();
            $table->string('value')->nullable();
            $table->string('value_float')->nullable();
            $table->string('name', 255)->nullable();
            $table->string('unit', 255)->nullable();
            $table->string('position', 255)->nullable();
            $table->string('group', 255)->nullable();

            $table->timestamps();

            $table->foreign('property_id')->references('id')->on('shop_properties');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_properties_values');
    }
}
