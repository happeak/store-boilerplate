require('./bootstrap');

import VueI18n from 'vue-i18n';
import locales from './vue-i18n-locales.generated.js';

const i18n = new VueI18n({
    locale: Laravel.locale,
    fallbackLocale: 'ru',
    messages: locales
});

let jsonp = require('jsonp');

Vue.component('cart-button', require('./components/cart/CartButton').default);
Vue.component('cart-widget', require('./components/cart/CartWidget').default);

Vue.prototype.$eventHub = new Vue();

new Vue({
    el: '#app',
    i18n,
    mounted() {
        let url = 'https://ecom.happeak.ru/locale/' + Laravel.locale + '/jsonp';
        let currentLocale = localStorage.getItem('locale');

        if ('null' === currentLocale) {
            jsonp(url, {}, function (error, data) {
                if (error) {
                    console.log(error);
                }

                localStorage.setItem('locale', data.locale);
            });
        }

    },
});