export const validatorConfig = {
    locale: 'ru',
    events: '',
    dictionary: {
        ru: {
            messages: {
                _default: function (e) {
                    return "Некорректный формат"
                },
                alpha_dash: function (e) {
                    return "Значение поля может содержать буквы, цифры, дефисы и подчеркивания"
                },
                alpha_num: function (e) {
                    return "Значение поля может содержать только буквы и цифры"
                },
                alpha_spaces: function (e) {
                    return "Значение поля может содержать только буквы и пробелы"
                },
                alpha: function (e) {
                    return "Значение поля может содержать только буквы"
                },
                email: function (e) {
                    return "Некорректный E-mail адрес"
                },
                integer: function (e) {
                    return "Значение поля должно быть целым числом"
                },
                ip: function (e) {
                    return "Введенный IP-адрес имеет некорректный формат"
                },
                length: function (e, n) {
                    var t = n[0],
                        a = n[1];
                    return a ? "The " + e + " length be between " + t + " and " + a + "." : "The " + e + " length must be " + t + "."
                },
                max: function (e, n) {
                    return "Длина поля не должна превышать " + n[0] + " символов."
                },
                max_value: function (e, n) {
                    return "Значение поля должно быть равно или менее  " + n[0]
                },
                mimes: function (e) {
                    return "Некорректный тип файла"
                },
                min: function (e, n) {
                    return "Количество символов в поле должно быть менее " + n[0]
                },
                min_value: function (e, n) {
                    return "Значение поля должно быть равно или больше чем " + n[0]
                },
                regex: function (e) {
                    return "Некорректный тип файла"
                },
                size: function (e, t) {
                    var a = t[0];
                    return "Размер поля должен быть меньше " + n(a) + "."
                },
                url: function (e) {
                    return "Некорректный формат URL"
                },
                confirmed: () => {
                    return 'Пароли не совпадают';
                },
                numeric: () => {
                    return 'Поле должно содержать только цифры';
                },
                required: () => {
                    return 'Поле обязательно для заполнения';
                }
            }
        }
    }
}