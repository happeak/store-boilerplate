import VueAlert from './Alert.vue';

export default {
    install (Vue) {
        Vue.component('v-alert', VueAlert);

        Object.defineProperties(Vue.prototype, {
            $alert: {
                get () {
                    let el = this;
                    while (el) {
                        for (let i = 0; i < el.$children.length; i++) {
                            const child = el.$children[i];

                            if (child.$options._componentTag === 'v-alert') {
                                return child;
                            }
                        }

                        el = el.$parent;
                    }

                    return null;
                }

            }
        })
    }
};