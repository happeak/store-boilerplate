<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">

    <title>{{ seo_title() }}</title>

    <meta name="description" content="{{ seo_description() }}">
    <meta name="keywords" content="{{ seo_keywords() }}">

    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        const Laravel = {
            locale: "{{ app()->getLocale() }}"
        };
    </script>
</head>
<body>

<div id="app"></div>

<script src="https://ecom.happeak.ru/js/cart.js"></script>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>