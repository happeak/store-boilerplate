@extends('backpack::layout')

@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="box box-default">
                <div class="box-header with-border">
                    <div class="box-title">{{ trans('backpack::base.login') }}</div>
                </div>
                <div class="box-body">
                    <a href="{{ route('oauth', ['provider' => 'google']) }}" class="btn btn-primary">
                        <i class="fa fa-google"></i> Войти с Google
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
