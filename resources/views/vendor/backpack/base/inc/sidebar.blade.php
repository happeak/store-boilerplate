@if (Auth::check())
    <aside class="main-sidebar">
        <section class="sidebar">
            @include('backpack::inc.sidebar_user_panel')

            <ul class="sidebar-menu">
                <li>
                    <a href="{{ backpack_url('dashboard') }}">
                        <i class="fa fa-dashboard"></i>
                        <span>{{ trans('backpack::base.dashboard') }}</span>
                    </a>
                </li>

                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/banners') }}">
                        <i class="fa fa-picture-o"></i>
                        <span>Баннеры</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/brands') }}">
                        <i class="fa fa-star"></i>
                        <span>Бренды</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/categories') }}">
                        <i class="fa fa-list"></i>
                        <span>Категории</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/models') }}">
                        <i class="fa fa-car"></i>
                        <span>Модели</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/products') }}">
                        <i class="fa fa-product-hunt"></i>
                        <span>Товары</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url(config('backpack.base.route_prefix').'/seo_parameters') }}">
                        <i class="fa fa-info-circle"></i>
                        <span>SEO параметры</span>
                    </a>
                </li>
            </ul>
        </section>
    </aside>
@endif
