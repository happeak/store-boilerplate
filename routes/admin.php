<?php

CRUD::resource('banners', 'BannerCrudController');
CRUD::resource('brands', 'BrandCrudController');
CRUD::resource('categories', 'CategoryCrudController');
CRUD::resource('models', 'ProductModelCrudController');
CRUD::resource('products', 'ProductCrudController');
CRUD::resource('seo_parameters', 'SeoParameterCrudController');