<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('catalog', function () {
    return view('welcome');
});

Route::group(['prefix' => 'oauth'], function () {
    Route::group(['prefix' => '{provider}'], function () {
        Route::get('/', [
            'as'   => 'oauth',
            'uses' => 'OauthController@redirectToProvider',
        ]);
        Route::get('callback', [
            'as'   => 'oauth_callback',
            'uses' => 'OauthController@handleProviderCallback',
        ]);
    });
});